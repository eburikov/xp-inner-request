<?php

namespace Xplatform\InnerRequest\Providers;

use Illuminate\Support\ServiceProvider;
use Xplatform\InnerRequest\InnerRequest;

class InnerRequestProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InnerRequest::class, function ($app) {
            return new InnerRequest();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__."/../../config/innerservices.php" => config_path('innerservices.php'),
        ], 'inner-request-service-config');
    }
}
