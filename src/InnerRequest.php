<?php
namespace Xplatform\InnerRequest;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Response;

class InnerRequest {
    private $services;

    public function __construct () {
        $this->services = config('innerservices');
    }

    public function request(String $method, String $service, String $route, Array $payload = null): Response {
        $response = Http::withHeaders(
            $this->setJWTHeader()
        )->{Str::lower($method)}(
            $this->getServiceUrl($service).'/api/'.$route,
            $payload
        );

        return response($response->json(), $response->status())
            ->withHeaders($response->headers());
    }

    private function getServiceUrl (String $service) {
        if (array_key_exists($service, $this->services)) {
            return $this->services[$service];
        };
        abort(404, 'Service '.$service.' is not implemented');
    }

    private function setJWTHeader () {
        $headers = request()->header();
        if (array_key_exists('x-api-key', $headers)) {
            return ['x-api-key' => $headers['x-api-key']];
        }
    }
}
